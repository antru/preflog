:- import add_lib_dir/1 from consult.
:- add_lib_dir(('../src')).
:- import gt/2, and/3, or/3, alt/2 from operators.

constant(a).
constant(b).
constant(c).
constant(d).
constant(e).
constant(two_lane).
constant(one_lane).
constant(dirt).

:- table ppath_/3.
ppath(X,Y,V) :- ppath_(X,Y,V), not ppath_bypassed(X,Y,V).
ppath_bypassed(X,Y,V) :- ppath_(X,Y,U), gt(U,V).
ppath_(X,Y,f(0)) :- constant(X), constant(Y).

:- table p_/3.
p(X,Y,V) :- p_(X,Y,V), not p_bypassed(X,Y,V).
p_bypassed(X,Y,V) :- p_(X,Y,U), gt(U,V).
p_(X,Y,f(0)) :- constant(X), constant(Y).

:- table edge_/4.
edge(X,Y,T,V) :- edge_(X,Y,T,V), not edge_bypassed(X,Y,T,V).
edge_bypassed(X,Y,T,V) :- edge_(X,Y,T,U), gt(U,V).
edge_(X,Y,T,f(0)) :- constant(X), constant(Y), constant(T).

ppath_(X,Y,V) :- p_(X,Y,V).
ppath_(X,Y,V) :- p_(X,Z,V1), ppath_(Z,Y,V2), and(V1,V2,V).

p_(X,Y,V) :- edge_(X,Y,two_lane,V1),
             edge_(X,Y,one_lane,V2), alt(V2,V3), or(V1,V3,V4), 
             edge_(X,Y,dirt,V5), alt(V5,V6), alt(V6,V7), or(V4,V7,V).

edge_(a,b,two_lane,t(0)).
edge_(a,c,one_lane,t(0)).
edge_(b,c,two_lane,t(0)).
edge_(c,d,one_lane,t(0)).
edge_(a,d,dirt,t(0)).
edge_(c,e,dirt,t(0)).
