:- import add_lib_dir/1 from consult.
:- add_lib_dir(('../src')).
:- import gt/2, opt/2, and/3 from operators.

constant(a).
constant(b).
constant(john).
constant(paul).

:- table likes_/3.
likes(P,O,V) :- likes_(P,O,V), not likes_bypassed(P,O,V).
likes_bypassed(P,O,V) :- likes_(P,O,U), gt(U,V).
likes_(P,O,f(0)) :- constant(P), constant(O).

:- table good_quality_/2.
good_quality(O,V) :- good_quality_(O,V), not good_quality_bypassed(O,V).
good_quality_bypassed(O,V) :- good_quality_(O,U), gt(U,V).
good_quality_(O,f(0)) :- constant(O).

:- table object_/2.
object(O,V) :- object_(O,V), not object_bypassed(O,V).
object_bypassed(O,V) :- object_(O,U), gt(U,V).
object_(O,f(0)) :- constant(O).

:- set_order_bound(7).

likes_(john,O,V):- good_quality_(O,V1), likes_(paul,O,V2), opt(V2,V3), and(V1,V3,V).
likes_(paul,O,V):- good_quality_(O,V1), likes_(john,O,V2), opt(V2,V3), and(V1,V3,V).

good_quality_(a,t(0)).

object_(a,t(0)).
object_(b,t(0)).
