:- import add_lib_dir/1 from consult.
:- add_lib_dir(('../src')).
:- import gt/2, howtrue/2 from operators.

constant(mary).
constant(tom).
constant(bob).
constant(drama).
constant(action).

:- table watch_/2.
watch(X,V) :- watch_(X,V), not watch_bypassed(X,V).
watch_bypassed(X,V) :- watch_(X,U), gt(U,V).
watch_(X,f(0)) :- constant(X).

:- table likes_/3.
likes(P,M,V) :- likes_(P,M,V), not likes_bypassed(P,M,V).
likes_bypassed(P,M,V) :- likes_(P,M,U), gt(U,V).
likes_(P,M,f(0)) :- constant(P), constant(X).

watch_(X,V) :- likes_(mary,X,V1), likes_(bob,X,V2), likes_(tom,X,V3),
               howtrue([V1,V2,V3],V).

likes_(mary,drama,t(0)).
likes_(bob,action,t(0)).
likes_(tom,drama,t(0)).
