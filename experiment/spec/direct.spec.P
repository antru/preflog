:- import add_lib_dir/1 from consult.
:- add_lib_dir(('../src')).
:- import gt/2, and/3, alt/2, or/3 from operators.

:- table flight/1, direct/1.
:- table external5/1, preferred5/1, winnow51/1, winnow52/1, bypassed51/1, bypassed52/1.

constant(athens).
constant(athens).
constant(london).
constant(london).
constant(rome).
constant(rome).
constant(toronto).
constant(toronto).
flight(((_h159339,_h159340),f(0))):- constant(_h159339),constant(_h159340).
direct(((_h159339,_h159340),f(0))):- constant(_h159339),constant(_h159340).
direct(((_h159339,_h159340),f(0))):- constant(_h159339),constant(_h159340).
flight(((_h159339,_h159340),_h159337)):- direct(((_h159339,_h159340),_h159348)),direct(((_h159339,_h159362),_h159359)),flight(((_h159362,_h159340),_h159370)),and(_h159359,_h159370,_h159380),alt(_h159380,_h159386),or(_h159348,_h159386,_h159337).
direct(((athens,rome),t(0))).
direct(((athens,rome),t(0))).
direct(((rome,london),t(0))).
direct(((rome,london),t(0))).
direct(((london,toronto),t(0))).
direct(((london,toronto),t(0))).

internal_pref5((_h159337,_h159338),(_h159337,_h159341)):- flight((_h159337,_h159338)),flight((_h159337,_h159341)),gt(_h159338,_h159341).
external5(_h159334):- winnow52(_h159334).
external_pref70((_h159337,_h159338),(_h159340,_h159341)):- external5((_h159337,_h159338)),external5((_h159340,_h159341)),gt(_h159338,_h159341).
preferred5(_h159334):- winnow51(_h159334).
winnow51(_h159334):- external5(_h159334),not bypassed51(_h159334).
winnow52(_h159334):- flight(_h159334),not bypassed52(_h159334).
bypassed51(_h159334):- external5(_h159339),external_pref70(_h159339,_h159334).
bypassed52(_h159334):- flight(_h159339),internal_pref5(_h159339,_h159334).
