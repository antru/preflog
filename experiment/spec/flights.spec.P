:- import add_lib_dir/1 from consult.
:- add_lib_dir('../src').
:- import gt/2, and/3, opt/2, or/3, alt/2 from operators.

:- table desired_flight/1, has_stopover/1, from_to/1, stopover/1, carrier/1.
:- table external1/1, preferred1/1, winnow11/1, winnow12/1, bypassed11/1, bypassed12/1.
:- table external2/1, preferred2/1, winnow21/1, winnow22/1, bypassed21/1, bypassed22/1.

constant(fl1).
constant(fl2).
constant(fl3).
constant(athens).
constant(boston).
constant(london).
constant(rome).
constant(aegean).
constant(british).

desired_flight((_h34313,f(0))):- constant(_h34313).
has_stopover((_h34313,f(0))):- constant(_h34313).
from_to(((_h34316,_h34319,_h34320),f(0))):- constant(_h34316),constant(_h34319),constant(_h34320).
stopover(((_h34316,_h34317),f(0))):- constant(_h34316),constant(_h34317).
stopover(((_h34316,_h34317),f(0))):- constant(_h34316),constant(_h34317).
carrier(((_h34316,_h34317),f(0))):- constant(_h34316),constant(_h34317).

desired_flight((_h34313,_h34314)):- from_to(((athens,boston,_h34313),_h34322)),has_stopover((_h34313,_h34336)),and(_h34322,_h34336,_h34343),carrier(((_h34313,aegean),_h34351)),opt(_h34351,_h34360),and(_h34343,_h34360,_h34314).
has_stopover((_h34313,_h34314)):- stopover(((_h34313,rome),_h34322)),stopover(((_h34313,london),_h34333)),alt(_h34333,_h34342),or(_h34322,_h34342,_h34314).

from_to(((athens,boston,fl1),t(0))).
from_to(((athens,boston,fl2),t(0))).
from_to(((athens,boston,fl3),t(0))).
stopover(((fl1,rome),t(0))).
stopover(((fl1,rome),t(0))).
stopover(((fl2,london),t(0))).
stopover(((fl2,london),t(0))).
stopover(((fl3,rome),t(0))).
stopover(((fl3,rome),t(0))).
carrier(((fl1,british),t(0))).
carrier(((fl2,aegean),t(0))).
carrier(((fl3,aegean),t(0))).

internal_pref1((_h34314,_h34315),(_h34314,_h34318)):- desired_flight((_h34314,_h34315)),desired_flight((_h34314,_h34318)),gt(_h34315,_h34318).
external1(_h34311):- winnow12(_h34311).
external_pref1((_h34314,_h34315),(_h34317,_h34318)):- external1((_h34314,_h34315)),external1((_h34317,_h34318)),gt(_h34315,_h34318).
preferred1(_h34311):- winnow11(_h34311).
winnow11(_h34311):- external1(_h34311),not bypassed11(_h34311).
winnow12(_h34311):- desired_flight(_h34311),not bypassed12(_h34311).
bypassed11(_h34311):- external1(_h34316),external_pref1(_h34316,_h34311).
bypassed12(_h34311):- desired_flight(_h34316),internal_pref1(_h34316,_h34311).

internal_pref2((_h60501,_h60502),(_h60501,_h60505)):- has_stopover((_h60501,_h60502)),has_stopover((_h60501,_h60505)),gt(_h60502,_h60505).
external2(_h60498):- winnow22(_h60498).
external_pref2((_h60501,_h60502),(_h60504,_h60505)):- external2((_h60501,_h60502)),external2((_h60504,_h60505)),gt(_h60502,_h60505).
preferred2(_h60498):- winnow21(_h60498).
winnow21(_h60498):- external2(_h60498),not bypassed21(_h60498).
winnow22(_h60498):- has_stopover(_h60498),not bypassed22(_h60498).
bypassed21(_h60498):- external2(_h60503),external_pref2(_h60503,_h60498).
bypassed22(_h60498):- has_stopover(_h60503),internal_pref2(_h60503,_h60498).

