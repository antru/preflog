echo "
  [experiment].
  create_facts.
" | ~/bin/XSB/bin/xsb

cat  ../results/flights.out.P  ./db/pl/flights.db.P  >  flights.prolog.P
cat  ../results/paths.out.P    ./db/pl/paths.db.P    >  paths.prolog.P
cat  ../results/direct.out.P   ./db/pl/direct.db.P   >  direct.prolog.P
cat  ../results/cyclic.out.P   ./db/pl/cyclic.db.P   >  cyclic.prolog.P
cat  ../results/howtrue.out.P  ./db/pl/howtrue.db.P  >  howtrue.prolog.P

cat  ../hilog/flights.hilog.P  ./db/hl/flights.db.P  >  flights.hilog.P
cat  ../hilog/paths.hilog.P    ./db/hl/paths.db.P    >  paths.hilog.P
cat  ../hilog/direct.hilog.P   ./db/hl/direct.db.P   >  direct.hilog.P
cat  ../hilog/cyclic.hilog.P   ./db/hl/cyclic.db.P   >  cyclic.hilog.P
cat  ../hilog/howtrue.hilog.P  ./db/hl/howtrue.db.P  >  howtrue.hilog.P

cat  ./spec/flights.spec.P     ./db/hl/flights.db.P  >  flights.spec.P
cat  ./spec/paths.spec.P       ./db/hl/paths.db.P    >  paths.spec.P
cat  ./spec/direct.spec.P      ./db/hl/direct.db.P   >  direct.spec.P
cat  ./spec/cyclic.spec.P      ./db/hl/cyclic.db.P   >  cyclic.spec.P
cat  ./spec/howtrue.spec.P     ./db/hl/howtrue.db.P  >  howtrue.spec.P

echo "
  [experiment].
  run_experiments.
" | ~/bin/XSB/bin/xsb | tee output.txt

cp output.txt ./output/output_`date "+%Y-%m-%d_%T"`.txt

rm *.prolog.P *.hilog.P *.spec.P *.xwam
