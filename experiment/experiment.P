%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- import term_to_atom/2 from string.
:- import append/3 from basics.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

create_facts :-
  write('Creating pl facts'), nl,
  create_pl_facts_flights('./db/pl/flights.db.P', 100),
  create_pl_facts_paths('./db/pl/paths.db.P', 2, 3),
  create_pl_facts_direct('./db/pl/direct.db.P', 5, 5),
  create_pl_facts_cyclic('./db/pl/cyclic.db.P', 5),
  create_pl_facts_howtrue('./db/pl/howtrue.db.P', 100),
  write('Creating hl facts'), nl,
  create_hl_facts('./db/pl/flights.db.P', './db/hl/flights.db.P'),
  create_hl_facts('./db/pl/paths.db.P', './db/hl/paths.db.P'),
  create_hl_facts('./db/pl/direct.db.P', './db/hl/direct.db.P'),
  create_hl_facts('./db/pl/cyclic.db.P', './db/hl/cyclic.db.P'),
  create_hl_facts('./db/pl/howtrue.db.P', './db/hl/howtrue.db.P'),
  write('OK'), nl.

run_experiments :-
  write('Running experiments'), nl,
  run_experiment('flights', desired_flight(_,t(0)), preferred1(_)),
  run_experiment('flights', has_stopover(_,t(0)),   preferred2(_)),
  run_experiment('paths',   p(_,_,t(0)),            preferred3(_)),
  run_experiment('paths',   ppath(_,_,t(0)),        preferred4(_)),
  run_experiment('direct',  flight(_,_,t(0)),       preferred5(_)),
  run_experiment('cyclic',  likes(_,_,0),           preferred6(_)),
  run_experiment('howtrue', watch(_,t(1)),          preferred7(_)),
  write('OK'), nl.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

run_experiment(FileName, Goal, SpecGoal) :-
  str_cat(FileName, '.prolog.P', PrologPath),
  str_cat(FileName, '.hilog.P', HilogPath),
  str_cat(FileName, '.spec.P', SpecPath),
  query_program(PrologPath, Goal, PrologResults),
  query_spec_program(SpecPath, SpecGoal, SpecResults),
  query_hilog_program(HilogPath, Goal, HilogResults),
  PrologResults = HilogResults,
  HilogResults = SpecResults, 
  write('Results match'),
  nl.
  
query_program(File, Goal, Results) :-
  consult('../src/operators.P'),
  reconsult(File),
  write('Executing Goal '),
  write(Goal),
  write(' in program '),
  write(File),
  nl,
  term_variables(Goal, VarList),
  statistics(walltime, _),
  setof(VarList, Goal, Results),
  statistics(walltime, [_,Time]),
  write('Execution Time: '),
  write(Time),
  write(' sec (prolog)'),
  nl.

query_hilog_program(File, Goal, Results) :-
  consult('../src/operators.P'),
  consult('../src/hilogpref.P'),
  reconsult(File),
  Goal ^=.. [Pred|_],
  HilogGoal = preferred(Pred)(T),
  write('Executing Goal '),
  write(HilogGoal),
  write(' in program '),
  write(File),
  nl,
  statistics(walltime, _),
  setof(T, apply(apply(preferred,Pred),(T,_)), Temp),
  statistics(walltime, [_,Time]),
  write('Execution Time: '),
  write(Time),
  write(' sec (hilog)'),
  nl,
  tl2ll(Temp, Results).

query_spec_program(File, Goal, Results) :-
  query_program(File, Goal, Temp).
  tt2ll(Temp, Results).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

create_pl_facts_flights(OutPath, N) :-
  write('Creating file '),
  write(OutPath),
  nl,
  open(OutPath, write, OutFileDesc, []),
  create_facts_flights(OutFileDesc, 0, N),
  close(OutFileDesc).

create_facts_flights(_, N, N).
create_facts_flights(F, M, N) :-
  MM is M+1,
  str_catN(fl1, MM, Fl1),
  str_catN(fl2, MM, Fl2),
  str_catN(fl3, MM, Fl3),
  str_catN(fl4, MM, Fl4),
  write_fact(F, constant(Fl1)),
  write_fact(F, constant(Fl2)),
  write_fact(F, constant(Fl3)),
  write_fact(F, constant(Fl4)),
  write_fact(F, from_to_(athens,boston,Fl1,t(0))),
  write_fact(F, from_to_(athens,boston,Fl2,t(0))),
  write_fact(F, from_to_(athens,boston,Fl3,t(0))),
  write_fact(F, from_to_(athens,boston,Fl4,t(0))),
  write_fact(F, stopover_(Fl1,rome,t(0))),
  write_fact(F, stopover_(Fl2,rome,t(0))),
  write_fact(F, stopover_(Fl3,london,t(0))),
  write_fact(F, stopover_(Fl4,london,t(0))),
  write_fact(F, carrier_(Fl1,british,t(0))),
  write_fact(F, carrier_(Fl2,aegean,t(0))),
  write_fact(F, carrier_(Fl3,british,t(0))),
  write_fact(F, carrier_(Fl4,aegean,t(0))),
  create_facts_flights(F, MM, N).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

create_pl_facts_paths(OutPath, M, N) :-
  write('Creating file '),
  write(OutPath),
  nl,
  open(OutPath, write, OutFileDesc, []),
  create_facts_paths_cons_outer(OutFileDesc, 0, M, N),
  create_facts_paths_edge_outer(OutFileDesc, 0, M, N),
  close(OutFileDesc).


create_facts_paths_cons_outer(_, M, M, _).
create_facts_paths_cons_outer(F, K, M, N) :-
  create_facts_paths_cons_inner(F, 0, K, N),
  L is K+1,
  create_facts_paths_cons_outer(F, L, M, N).

create_facts_paths_cons_inner(_, N, _, N).
create_facts_paths_cons_inner(F, K, M, N) :-
  str_catMN(a, M, K, A),
  str_catMN(b, M, K, B),
  str_catMN(c, M, K, C),
  str_catMN(d, M, K, D),
  str_catMN(e, M, K, E),
  write_fact(F, constant(A)),
  write_fact(F, constant(B)),
  write_fact(F, constant(C)),
  write_fact(F, constant(D)),
  write_fact(F, constant(E)),
  KK is K+1,
  create_facts_paths_cons_inner(F, KK, M, N).


create_facts_paths_edge_outer(_, M, M, _).
create_facts_paths_edge_outer(F, K, M, N) :-
  create_facts_paths_edge_inner(F, 0, K, N),
  L is K+1,
  create_facts_paths_edge_outer(F, L, M, N).

create_facts_paths_edge_inner(_, N, _, N).
create_facts_paths_edge_inner(F, K, M, N) :-
  str_catMN(a, M, K, A),
  str_catMN(b, M, K, B),
  str_catMN(c, M, K, C),
  str_catMN(d, M, K, D),
  str_catMN(e, M, K, E),
  write_fact(F, edge_(A,B,two_lane,t(0))),
  write_fact(F, edge_(A,C,one_lane,t(0))),
  write_fact(F, edge_(B,C,two_lane,t(0))),
  write_fact(F, edge_(C,D,one_lane,t(0))),
  write_fact(F, edge_(A,D,dirt,t(0))),
  write_fact(F, edge_(C,E,dirt,t(0))),
  KK is K+1,
  create_facts_paths_edge_connect(F, K, KK, M, N),
  create_facts_paths_edge_inner(F, KK, M, N).

create_facts_paths_edge_connect(_, _, N, _, N).
create_facts_paths_edge_connect(F, K, L, M, _) :-
  str_catMN(e, M, K, E),
  str_catMN(a, M, L, A),
  write_fact(F, edge_(E,A,two_lane,t(0))).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

create_pl_facts_direct(OutPath, M, N) :-
  write('Creating file '),
  write(OutPath),
  nl,
  open(OutPath, write, OutFileDesc, []),
  create_facts_direct_cons_outer(OutFileDesc, 0, M, N),
  create_facts_direct_edge_outer(OutFileDesc, 0, M, N),
  close(OutFileDesc).


create_facts_direct_cons_outer(_, M, M, _).
create_facts_direct_cons_outer(F, K, M, N) :-
  create_facts_direct_cons_inner(F, 0, K, N),
  L is K+1,
  create_facts_direct_cons_outer(F, L, M, N).

create_facts_direct_cons_inner(_, N, _, N).
create_facts_direct_cons_inner(F, K, M, N) :-
  str_catMN(airport, M, K, Airport),
  write_fact(F, constant(Airport)),
  KK is K+1,
  create_facts_direct_cons_inner(F, KK, M, N).


create_facts_direct_edge_outer(_, M, M, _).
create_facts_direct_edge_outer(F, K, M, N) :-
  create_facts_direct_edge_inner(F, 1, K, N),
  L is K+1,
  create_facts_direct_edge_outer(F, L, M, N).

create_facts_direct_edge_inner(_, N, _, N).
create_facts_direct_edge_inner(F, K, M, N) :-
  L is K-1,
  str_catMN(airport, M, L, Airport1),
  str_catMN(airport, M, K, Airport2),
  write_fact(F, direct_(Airport1,Airport2,t(0))),
  KK is K+1,
  create_facts_direct_edge_inner(F, KK, M, N).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

create_pl_facts_cyclic(OutPath, N) :-
  write('Creating file '),
  write(OutPath),
  nl,
  open(OutPath, write, OutFileDesc, []),
  create_facts_cyclic(OutFileDesc, 0, N),
  close(OutFileDesc).

create_facts_cyclic(_, N, N).
create_facts_cyclic(F, M, N) :-
  MM is M+1,
  str_catN(a, MM, A),
  str_catN(b, MM, B),
  write_fact(F, constant(A)),
  write_fact(F, constant(B)),
  write_fact(F, good_quality_(A,t(0))),
  write_fact(F, object_(A,t(0))),
  write_fact(F, object_(B,t(0))),
  create_facts_cyclic(F, MM, N).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

create_pl_facts_howtrue(OutPath, N) :-
  write('Creating file '),
  write(OutPath),
  nl,
  open(OutPath, write, OutFileDesc, []),
  create_facts_howtrue(OutFileDesc, 0, N),
  close(OutFileDesc).

create_facts_howtrue(_, N, N).
create_facts_howtrue(F, M, N) :-
  MM is M+1,
  str_catN(drama, MM, Drama),
  str_catN(action, MM, Action),
  write_fact(F, constant(Drama)),
  write_fact(F, constant(Action)),
  write_fact(F, likes_(mary,Drama,t(0))),
  write_fact(F, likes_(bob,Action,t(0))),
  write_fact(F, likes_(tom,Drama,t(0))),
  create_facts_howtrue(F, MM, N).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

create_hl_facts(PlPath, HlPath) :-
  write('Creating file '),
  write(HlPath),
  nl,
  open(PlPath, read, PFile, []),
  open(HlPath, write, HFile, []),
  read_term(PFile, FstFact, []),
  create_hl_facts_rec(PFile, HFile, FstFact),
  close(HFile),
  close(PFile).

create_hl_facts_rec(_, _, end_of_file) :- !.
create_hl_facts_rec(PFile, HFile, OuterFact) :-
  conv_fact(OuterFact, NewOuterFact),
  write_fact(HFile, NewOuterFact),
  read_term(PFile, ThisFact, []),
  create_hl_facts_rec(PFile, HFile, ThisFact).

conv_fact(constant(X), constant(X)).
conv_fact(from_to_(X,Y,Z,t(0)), from_to(((X,Y,Z),t(0)))).
conv_fact(stopover_(X,Y,t(0)), stopover(((X,Y),t(0)))).
conv_fact(carrier_(X,V,t(0)), carrier(((X,V),t(0)))).
conv_fact(edge_(X,Y,Z,t(0)), edge(((X,Y,Z),t(0)))).
conv_fact(direct_(X,V,t(0)), direct(((X,V),t(0)))).
conv_fact(good_quality_(X,t(0)), good_quality((X,t(0)))).
conv_fact(object_(X,t(0)), object((X,t(0)))).
conv_fact(likes_(X,V,t(0)), likes(((X,V),t(0)))).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

str_cat3(A,B,C,R) :-
  str_cat(A,B,T),
  str_cat(T,C,R).

str_catN(A,N,R) :-
  term_to_atom(N,NN),
  str_cat(A,NN,R).

str_catMN(A,M,N,R) :-
  term_to_atom(M,MM),
  term_to_atom(N,NN),
  str_cat3(A,MM,NN,R).

tl2ll([], []).
tl2ll([T|Ts], [[T]|Ls]) :- atom(T), tl2ll(Ts, Ls).
tl2ll([T|Ts], [L|Ls]) :- T ^=.. [_|L], tl2ll(Ts, Ls).

tt2ll([], []).
tt2ll([(T,_)|Ts], [[T]|Ls]) :- atom(T), tt2ll(Ts, Ls).
tt2ll([[((X,Y),_)]|Ts], [[X,Y]|Ls]) :- tt2ll(Ts, Ls).

write_fact(File, X) :-
  write_term(File, X, []),
  writeln(File, '.').
